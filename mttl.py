#!/usr/bin/python3

import os
import argparse
import pymongo
import subprocess


HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def clear_mongo_database(client):
    return client.drop_database('mttl')

def before_script():
    cmd = 'scripts/before_script.sh'
    return subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def after_script():
    cmd = 'scripts/after_script.sh'
    return subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def main():
    client = pymongo.MongoClient(HOST, PORT)

    cmds = ['find','insert','modify','delete']
    parser = argparse.ArgumentParser(
                        description=f'entrance script to {"/".join(cmds)} ksat/rel-link data')
    parser.add_argument('cmd', choices=cmds,
                        help='all Commands you can run')
    parser.add_argument('args', nargs=argparse.REMAINDER,
                        help='arguments for command')
    parsed_args = parser.parse_args()

    script = [os.path.join('scripts', 'mttl_cmds', parsed_args.cmd + '.py')] + parsed_args.args

    # drop current database and seed new one
    clear_mongo_database(client)
    before_script()

    script_proc = subprocess.Popen(script)
    script_proc.communicate()

    # export the changes to the ksat files
    after_script()
    exit(script_proc.returncode)
    

if __name__ == "__main__":
    main()