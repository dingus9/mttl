
# Training Roadmap Layout
* The training roadmap takes the form of a graph which each node representing a work role in the 90th COS.
* The work roles on the farthest left side are work roles that do not have another workrole as a prerequisite.
* The lines connecting work roles represent the available paths one can take to transition from one work role to another.
* If a work role does not have any lines originating from the right side are work roles that do not lead to more advanced work roles.
* Specializations are displayed as work roles in the training roadmap diagram.

# Required Items
* Clicking the name of a work role will display a list of items needed to complete that work role. These items will be required courses and/or required evaluation.
* Items in the work role list can be clicked to navigate to a page to sign up for any course or evaluation.
* If there is additional supplementary content for a required item, the arrow next to the item will show additonal content for that role. Clicking these links will take you to that content for review.

# OJT Printout
A list of On the Job training items can be obtained by clicking the OJT option on a given work role. Additional information on the OJT checklist can be found [here](how-to-find-and-accomplish-ojt.md)