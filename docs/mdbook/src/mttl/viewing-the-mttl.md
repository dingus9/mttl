# Viewing the MTTL

The 90COS MTTL can be viewed [here](https://90cos.gitlab.io/mttl/#/). Below is an example of the MTTL web page.

The Master Training Task List (MTTL) frontend is composed of three pages.

* The main page - This page shows a coverage report for a work role, a chart showing all Knowledge, Skill, Ability, Task (KSAT) IDs aligned to each work role and specialization, and a list of KSAT IDs that are not aligned to any work role.

* The Metrics page - This page shows a numeric overview of indicating coverage for training, evaluation, and both training and evaluation for each work role as well as all work roles.

* The Training Roadmap page displays a diagram showing the required courses for each work role and which work roles act as prerequisites to more advanced work roles and specializations. 

### Main Page
The first Item on the Main page is a color coded coverage report displaying all KSATs along with links to the training items and evaluation items that provide coverage for that item (if any). Coverage means that there is content that either provides instruction on that KSAT (training) or tests someone that they meet the requirement specified by the KSAT (evaluation). Items that have both training and evaluation coverage will be colored green. Items with only training coverage will be blue. Items with only evaluation coverage will be yellow. Any items that have neither training nor evaluation coverage will be black. Items that do not have full coverage are assumed to be met via on the job training.

Each entry will show a list of information that includes a unique ID; a description; related KSATs; a list of work roles and specializations that use that KSAT; and links to the covering content, if any. The Table Select component in the top right of the page will filter the KSATs by work role. The search bar will return results containing the text in the search. The controls below the Table Select will toggle fullscreen mode, allow you to omit columns from the display, or print the contents of the display, respectively. Below the display you can set how many items you wish to display at once.

### Metrics Page
This page will show overall coverage reports. The percentage shown is the percentage of items with either both training and evaluation, training only, or evaluation only coverage. A coverage of 100% means all KSATs of that work role have the correlating coverage.

### Training Roadmap
The diagram is divided into work roles; the work roles on the leftmost side have no prerequisite work roles. These roles can lead into more advanced roles, with a red line illustrating which roles connect to each other. Inside each role is a list of courses which can be viewed by clicking the button at the top of the node. This will display a list of all requirements to be completed. Clicking the arrow next to the name of a requirement will display a list of all modules included in that course along with links to the material. If there are any on-the-job training (OJT) items that must be completed, the OJT link in the node will lead to a printable list of all OJT items. This list is to be the primary method of tracking OJT. 

![images_mttl](../uploads/mttl_home.png)

- The bar with the percentage indicates the percentage of KSATs being covered by both training and evaluations. 
- The legend in the upper-right corner of the page describes the color mappings to indicate whether a KSAT is covered by evaluations and/or training.
- The 'Table Select' button allows you to view the KSATs for a single work role.

The following are descriptions of the MTTL fields:

| Field | Explanation |
| ------------| ------------|
| **ID** | The KSAT identified.  KSAT IDs begin with an alpha character to indicate its category: <br><br> T - Task, A - Ability, S - Skill, K - Knowledge |
| **Source** | An original source document that identified this KSAT |
| **Owner** | The organization that originated the KSAT |
| **Parent(s)** | Parents are KSATs that this KSAT supports. This KSAT is a building block for the parent(s) KSATs |
| **Description** | A description of the task, ability, skill, or knowledge needed to satisfy this KSAT |
| **Children** | This is the reciprocal of the Parent(s) field and shows all the children that support this KSAT |
| **Work Role/Specialization** | This is the 90COS defined work role or specialization. These are defined in the [Work Role and Specification table](#work-roles-and-specializations) above. |
| **Topic** | The topic or subject related to the KSAT such as Python, Networking, Assembly, etc. |
| **Training Covered** | A list of links of training that cover this topic. Clicking a listed item will redirect you to the training material |
| **Eval Covered** | A list of links of evaluations that cover this topic. Clicking a listed item will redirect you to the evaluation material |