# Contributing to the MTTL

Anybody with a 90COS gitlab account can modify or add to the MTTL. Any changes or additions to the MTTL will be reviewed and approved or rejected by a designated Subject Matter Expert (SME) for the associated work role changes apply to.

The MTTL consists of four distinct .json files described in the [MTTL Data Dictionary](https://gitlab.com/90cos/mttl/-/wikis/Home#data-file-directory-structure). An example of a single KSAT definition inside this file is shown and described below. Edit or add KSATs to these JSON files following the [Change Management Workflow](https://gitlab.com/90cos/mttl/-/wikis/Home#change-management-workflow) documented on the MTTL Home page. 

| Field | Required? | Description |
|-------|-----------|---------|
| _id | required | The KSAT id number. When adding a new KSAT, add to the bottom of the page and use the next available number sequence |
| description | required | Reference [Bloom's Taxonomy](https://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy/) to help write meaningful KSAT descriptions. This taxonomy was created specifically for teaching, learning, and assessment. The taxonomy focuses on using verbs and gerunds to label categories and subcategories. *These “action words” describe the cognitive processes by which thinkers encounter and work with knowledge.* |
| parent | optional | List all parents related to this KSAT.  A parent KSAT is typically a broader KSAT that this KSAT supports. This KSAT represents an essential building-block for its parent(s) KSAT. Reference the [KSAT parent-child](ksat-parent-child-rels.md) for more information on this relationship. |
| topic | required | Provide a topic this KSAT supports. |
| requirement_src | required | List one or more requirement documents that originated this KSAT. These are for KSATs originated external to the 90COS |
| requirement_owner | required |Provide the office of primary responsibility (OPR) for this KSAT.  These are for KSATs originated external to the 90COS |
| comments | optional | Provide any additional supporting comments. |
| training | optional | A list of object ids.  Each object id references a training component that addresses this KSAT. This will automatically be populated when a training rel-link file is updated with this KSAT |
| eval | optional | A list of object ids.  Each object id references a evaluation question/problem that addresses this KSAT. This will automatically be populated when a eval rel-link file is updated with this KSAT |
| updated_on | optional | The date this KSAT was last modified.  This is automatically updated by the pipeline |

```json
{
	"_id": "S0028",
	"description": "Set up and replicate a Python Virtual Environment.",
	"parent": [
		"T0010"
	],
	"topic": "Python",
	"requirement_src": [
		"ACC CCD CDD TTL"
	],
	"requirement_owner": "ACC A2/3/6K",
	"comments": "",
	"training": [],
	"eval": [],
	"updated_on": "2020-06-16"
}
```
