# How to use the MTTL
Questions to be answered.

1. What is a KSAT?
1. How do I search through the MTTL to find ones that might exist before I submit a new one?
1. How do I write a good KSAT using Bloom's taxonomy?
1. How do I know what proficiency code to use?
1. What is a parent? Source? and why is this important?
1. How do I specify a source correctly?

### Master Training Task List

The 90COS Master Training Task List (MTTL) is a repository of knowledge, skills, abilities, and tasks (KSATs) determined as qualification requirements for each work role or specialization in the organization. One or more work roles or specializations are assigned to each KSAT.

### Work Roles and Specializations

| Work Role | Description |
|------------|------------|
| Basic-Dev | Basic Capabilities Developer |
| Senior-Dev-Windows | Senior Windows Capabilities Developer |
| Senior-Dev-Linux | Senior Linux Capabilities Developer |
| Test-Automation-Engineer | Test Automation Engineer | 
| Embedded | Embedded Developer |
| Network | Network Engineer |
| Mobile | Mobile Application Developer |
| Basic-Po | Basic Product Owner |
| Senior-Po | Senior Product Owner |
| Everyone | Applies to everyone in 90COS |

| Specialization | Description |
|------------|------------| 
| SEE | Stan/Eval Evaluator |