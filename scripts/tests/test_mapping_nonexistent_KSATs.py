#!/usr/bin/python3

import os
import sys
import json
import pymongo
from bson import ObjectId


HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links

filename = os.path.splitext(os.path.basename(__file__))[0]
log_data = {}
error = False

def verify_ksat_exists(ksat_id: str):
    global error, reqs
    if reqs.count_documents({'_id': ksat_id}) > 0:
        return True
    return False

def verify_mapping_exists(rel_link: dict):
    for mapping in rel_link['KSATs']:
        if not verify_ksat_exists(mapping['ksat_id']):
            _id = str(rel_link['_id'])
            if _id not in log_data:
                log_data[_id] = {}
            log_data[_id].update({
                mapping['ksat_id']: 'does not exist in requirements'
            })

def main():
    global error, rls
    # loop all rel_links
    for rel_link in rls.find({}):
        verify_mapping_exists(rel_link)

    if len(log_data) > 0:
        with open(f'{filename}.json', 'w') as log_file:
            json.dump(log_data, log_file, sort_keys=False, indent=4) 
        exit(1)

if __name__ == "__main__":
    main()