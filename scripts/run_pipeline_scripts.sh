#!/bin/bash

rm -rf frontend/src/data
mongo --host $MONGO_HOST --eval 'db.dropDatabase()' mttl
./scripts/before_script.sh

./scripts/pre_build/create_mttl_children.py
./scripts/pre_build/create_mttl_links.py
./scripts/build/build_data.py WORK-ROLES.json SPECIALIZATIONS.json -o frontend/src/data
./scripts/build/create_mttl_dataset.py
./scripts/build/create_ttl_dataset.py
./scripts/build/create_extra_datasets.py
./scripts/build/create_metrics.py

./scripts/build/create_jqs_per_workrole.py -wr Basic-Dev
./scripts/build/create_jqs_per_workrole.py -wr Basic-PO
./scripts/build/create_jqs_per_workrole.py -wr Senior-PO
./scripts/build/create_jqs_per_workrole.py -wr Senior-Dev-Windows
./scripts/build/create_jqs_per_workrole.py -wr Senior-Dev-Linux
./scripts/build/create_jqs_per_workrole.py -wr Test-Automation-Engineer