#!/usr/bin/python3

import os
import sys
import json
import jsonschema
import pymongo
import argparse
from datetime import date
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from json_templates import ksat_item_template, ksat_list_template, trn_rel_link_item_template, evl_rel_link_item_template


HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))
    
missed_items = []    


def find_id_and_append(collection, find_id: str, update_field: str, value: [str, dict], upsert:bool=False):
    '''
    MongoDB Query function to find one document with id
    '''
    return collection.update_one(
        { '_id': find_id },
        { 
            '$addToSet': { update_field: value },
            '$set': { 'updated_on': f'{date.today()}' }
        },
        upsert=upsert
    )

def key_picker(id: str):
    return {
        'knowledge': 'K',
        'skills': 'S',
        'abilities': 'A',
        'tasks': 'T'
    }[id.lower()]

def get_insert_data(path: str) -> list:
    '''
    load and return the batch file with insertion data
    '''
    data = None
    with open(path, 'r') as path_file:
        data = json.load(path_file)
    if type(data) is dict:
        data = [data]
    return data

def get_work_roles(paths: list) -> list:
    '''
    return a list of all the work-roles/specializations
    '''
    wr_list = []
    for path in paths:
        with open(path, 'r') as path_file:
            wr_list += list(json.load(path_file).keys())
    return wr_list

def get_new_ksat_key(collection: object, ksat_type:str):
    '''
    Find the highest ksat id of a specific ksat type, increment, and return
    '''
    ksat_tag = key_picker(ksat_type)
    ksat_id = collection.find_one({
        '_id' : {'$regex': f'[{ksat_tag}][0-9]{{4}}'}
    }, sort=[('_id', -1)])['_id']
    num = ksat_id[1:]
    return f'{ksat_tag}{(int(num) + 1):04d}'

def validate_insert_data(data: dict, schema_func):
    '''
    validate all insertion data
    '''
    wr_list = get_work_roles(['WORK-ROLES.json', 'SPECIALIZATIONS.json'])
    try: 
        jsonschema.validate(instance=data, schema=schema_func(wr_list))
    except jsonschema.exceptions.ValidationError as err:
        print(err)

def insert_ksat_data(reqs: object, data: dict):
    '''
    insert the data into the ksat datasets
    '''
    # create empty ksat fields that are not specified in the batch.json
    ksat = {"_id": "", "parent": [], "requirement_src": [], "requirement_owner": "", "comments": "", "training": [],
        "eval": []}
    ksat.update(data)
    ksat['created_on'] = f'{date.today()}'

    # get new ksat id for insert
    if 'ksat_type' in ksat:
        ksat['_id'] = get_new_ksat_key(reqs, data['ksat_type'])
        del ksat['ksat_type'] # type is removed because it's not needed

    validate_insert_data(ksat, ksat_item_template)    
    reqs.insert_one(ksat)

    return ksat['_id']

def create_rel_link_file(parent_path: str, file_name: str):
    # create enpty file for each test_id/course if it does not exist already
    # this is necessary to make sure the mongoexport knows how to export
    if not os.path.exists(parent_path):
        os.makedirs(parent_path)
    if not os.path.exists(os.path.join(parent_path, file_name)):
        with open(os.path.join(parent_path, file_name), 'w') as rel_link_file:
            rel_link_file.write("")  

def insert_rel_link_data(reqs: object, rls: object, data: dict):
    '''
    insert the data into the rel_link datasets
    '''
    global missed_items
    rel_link = {}
    rel_link.update(data)
    rel_link['created_on'] = f'{date.today()}'       

    for mapping in rel_link['KSATs']:
        if reqs.count_documents({'_id': mapping['ksat_id']}) == 0:
            missed_items.append(data)
            return

    insert_id = None
    if rel_link['map_for'] == 'eval':
        create_rel_link_file(os.path.join('rel-links', 'eval'),  f'{data["test_id"]}.rel-links.json')
        validate_insert_data(data, evl_rel_link_item_template)
        insert_id = rls.insert_one(rel_link).inserted_id
    else:
        create_rel_link_file(os.path.join('rel-links', 'training'),  f'{data["course"]}.rel-links.json')
        validate_insert_data(data, trn_rel_link_item_template)
        insert_id = rls.insert_one(rel_link).inserted_id   
    
    for item in rel_link['KSATs']:
        find_id_and_append(reqs, item['ksat_id'], rel_link['map_for'], insert_id)

    return insert_id


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    global missed_items
    parser = argparse.ArgumentParser(
                        description=f'insert script to add ksat/rel-link data to datasets')
    parser.add_argument('-f', '--file', type=str,
                        default='batch.json', help='specify import file location (default = batch.json)')
    parser.add_argument('type', choices=['ksat', 'rel-link'],
                        default='batch.json', help='select the type of items you want to insert')
    parsed_args = parser.parse_args()

    data = get_insert_data(parsed_args.file)
    for item in data:
        if parsed_args.type == 'ksat':
            insert_ksat_data(reqs, item)
        else:
            insert_rel_link_data(reqs, rls, item)

    if len(missed_items) > 0:
        print(f'There were rel-links that map to nonexistent KSATs. All rel-links with incorrect mappings outputted to \'rel_links_w_errors.json\'')
        with open(f'rel_link_w_errors.json', 'w') as error_file:
            json.dump(missed_items, error_file, sort_keys=False, indent=4)


if __name__ == "__main__":
    main()