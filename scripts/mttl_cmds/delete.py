#!/usr/bin/python3

import os
import re
import pymongo
import argparse
from datetime import date
from bson.objectid import ObjectId

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def regex_type_validation(arg_value, ksat_pat=re.compile(r"^[KSAT][0-9]{4}$"), rel_link_pat=re.compile(r"^[0-9a-f]{24}$")):
    if not ksat_pat.match(arg_value) and rel_link_pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value

def delete_ksat_data(reqs: object, rls: object, ksat_id: str):
    ret = reqs.delete_one({'_id': ksat_id})
    if ret.deleted_count < 1:
        print(f'No KSAT associated with {ksat_id}')
        exit(1)

    # remove ksat_id from all rel_links that maps to it
    rls.update_many(
        {'KSATs.ksat_id': ksat_id},
        {
            '$pull': {'KSATs': {'ksat_id': ksat_id}},
            '$set': { 'updated_on': f'{date.today()}' }
        }
    )

def delete_rel_link_data(reqs: object, rls: object, id: str):
    rel_link = rls.find_one({'_id': ObjectId(id)})
    if rel_link != None:
        # remove rel-link id on all KSATs that have it mapped
        reqs.update_many(
            { '_id': {'$in': [_id['ksat_id'] for _id in rel_link['KSATs']]}},
            { 
                '$pull': { rel_link['map_for']: rel_link['_id'] },
                '$set': { 'updated_on': f'{date.today()}' }
            }
        )
        # remove the rel-link
        rls.delete_one({'_id': ObjectId(id)})
    else:
        print(f'No rel-link associated with {id}')

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    parser = argparse.ArgumentParser(
                        description=f'delete script to remove ksat/rel-link objects from datasets')
    parser.add_argument('type', choices=['ksat', 'rel-link'],
                        help='select what type of item you want to remove')
    parser.add_argument('id_list', metavar='_id', nargs='+', type=regex_type_validation,
                        help='list of either ksat or rel-link _id\'s')
    parsed_args = parser.parse_args()

    for item in parsed_args.id_list:
        if parsed_args.type == 'ksat':
            delete_ksat_data(reqs, rls, item)
        else:
            delete_rel_link_data(reqs, rls, item)

if __name__ == "__main__":
    main()