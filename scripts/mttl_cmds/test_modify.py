#!/usr/bin/python3

import os
import argparse
import unittest
import subprocess
import pymongo
import json
import random
from find import find_query
from modify import *

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


class TestMethods(unittest.TestCase):

    def test_insert_rel_link_mapping(self):
        num_items = 1
        rand_num = random.randrange(1, 100)
        test_rels = list(rls.find({'map_for': 'training'}).skip(rand_num).limit(num_items))
        test_ksats = list(reqs.find({}).skip(rand_num).limit(num_items))
        
        for i, test_rel in enumerate(test_rels):
            test_ksat = test_ksats[i]
            cmd = f"python3 mttl.py modify rel-link -i {str(test_rel['_id'])} {test_ksat['_id']} A test"
            subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            modified_rel_count = rls.count_documents({"_id": test_rel['_id'], "KSATs.ksat_id" : test_ksat['_id'], 'KSATs.url': 'test'})
            modified_ksat_count = reqs.count_documents({"_id": test_ksat['_id'], "training" : {'$all': [test_rel['_id']]}})
            # if modified count == 0 nothing was modified
            self.assertEqual(modified_rel_count, 1)
            # if modified count == 0 nothing was modified
            self.assertEqual(modified_ksat_count, 1)

    def test_modify_mapping_url(self):
        num_items = 1
        rand_num = random.randrange(1, 100)
        test_rels = list(rls.find({'KSATs': {'$not': {'$size': 0}}}).skip(rand_num).limit(num_items))
        
        for test_rel in test_rels:
            find_query = json.dumps({'_id': str(test_rel['_id'])}, separators=(',', ':'))
            for mapping in test_rel['KSATs']:
                cmd = f"python3 mttl.py modify rel-link -r {find_query} {mapping['ksat_id']}:www.google.com"
                subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                modified_rel_count = rls.count_documents({"_id": test_rel['_id'], "KSATs.ksat_id" : mapping['ksat_id'], 'KSATs.url': 'www.google.com'})
                # if modified count == 0 nothing was modified
                self.assertEqual(modified_rel_count, 1)

    def test_modify_mapping_prof(self):
        num_items = 1
        rand_num = random.randrange(1, 100)
        test_rels = list(rls.find({'KSATs': {'$not': {'$size': 0}}}).skip(rand_num).limit(num_items))
        
        for test_rel in test_rels:
            find_query = json.dumps({'_id': str(test_rel['_id'])}, separators=(',', ':'))
            for mapping in test_rel['KSATs']:
                cmd = f"python3 mttl.py modify rel-link -p {find_query} {mapping['ksat_id']}:D"
                subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                modified_rel_count = rls.count_documents({"_id": test_rel['_id'], "KSATs.ksat_id" : mapping['ksat_id'], 'KSATs.item_proficiency': 'D'})
                # if modified count == 0 nothing was modified
                self.assertEqual(modified_rel_count, 1)

    def test_delete_mapping(self):
        rand_num = random.randrange(1, 100)
        test_rel = list(rls.find({'KSATs': {'$not': {'$size': 0}}, 'map_for': 'training'}).skip(rand_num).limit(1))[0]
        
        find_query = json.dumps({'_id': str(test_rel['_id'])}, separators=(',', ':'))
        cmd = f"python3 mttl.py modify rel-link -d {find_query}".split()
        for mapping in test_rel['KSATs']:
            cmd.append(mapping['ksat_id'])
            subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        modified_rel = rls.find_one({"_id": test_rel['_id']})
        # if modified KSATs len == 0 everything removed
        self.assertEqual(len(modified_rel['KSATs']), 0)
        for mapping in test_rel['KSATs']:
            modified_ksat_count = reqs.count_documents({'_id': mapping['ksat_id'], 'training': {'$all': [mapping['ksat_id']]}})
            # should not find anything because the ksat no longer has the mapping
            self.assertEqual(modified_ksat_count, 0)

    def test_modify_update_ksat_id(self):
        rand_num = random.randrange(1, 100)
        num_items = 1
        test_ksats = list(reqs.find({}).skip(rand_num).limit(num_items))

        old_ksats = [ksat['_id'] for ksat in test_ksats]
        new_ksats = []
        while len(new_ksats) < num_items:
            rand_ksat_id = random.randrange(2000, 9999)
            new_ksat = f'K{rand_ksat_id:4}'
            if reqs.count_documents({'_id': new_ksat}) == 0:
                new_ksats.append(new_ksat)

        
        cmd = 'python3 mttl.py modify ksat -k'.split()
        for i, old_ksat in enumerate(test_ksats):
            cmd.append(f'{old_ksat["_id"]}:{new_ksats[i]}')
        subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.assertEqual(reqs.count_documents({'_id': {'$in': old_ksats}}), 0)
        self.assertEqual(reqs.count_documents({'_id': {'$in': new_ksats}}), num_items)


if __name__ == "__main__":
    unittest.main()