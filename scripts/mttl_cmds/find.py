#!/usr/bin/python3

import os
import re
import sys
import pymongo
import argparse
import json
from pprint import pprint
from datetime import date
from bson.objectid import ObjectId

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def find_query(collection: object, type_str:str, find_query:dict, projection:dict, print_val=True):
    ret_docs = None
    projection_query = {}
    if type_str == 'rel-link':
        if '_id' in find_query:
            find_query['_id'] = ObjectId(find_query['_id'])
    if projection != None:
        for item in projection:
            projection_query.update({item: 1})
        ret_docs = collection.find(find_query, projection_query)
    else:
        ret_docs = collection.find(find_query)

    ret = []
    for item in ret_docs:
        ret.append(item)
        if print_val:
            print()
            pprint(item)
            print()
    return ret



def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    parser = argparse.ArgumentParser(
                        description=f'find script to print MTTL data to stdout')
    parser.add_argument('-p', '--projection', nargs='+', type=str,
                        default=None, help='only return these fields to stdout')
    parser.add_argument('type', choices=['ksat', 'rel-link'],
                        help='select the type of item you want to find')
    parser.add_argument('find_query', type=json.loads,
                        help="mongodb query to find document to output '{...}' (wrap KEYs and VALs in double quotes)")
    parsed_args = parser.parse_args()
    if parsed_args.type == 'ksat':
        ret = find_query(reqs, parsed_args.type, parsed_args.find_query, parsed_args.projection)
    else:
        ret = find_query(rls, parsed_args.type, parsed_args.find_query, parsed_args.projection)
    
    return ret

if __name__ == "__main__":
    main()