#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
import mongo_helpers

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
wrs = db.work_roles

output_root = 'frontend/src/data'
log_output_path = os.path.join(output_root, 'create_wrspec_dataset.log') 

filename = os.path.splitext(os.path.basename(__file__))[0]

def key_picker(ksa: str):
    return {
        'T': 'Tasks',
        'A': 'Abilities',
        'S': 'Skills',
        'K': 'Knowledge'
    }[ksa[0]]

def query_and_itterate_wrspec(wrspec:str, outdata:dict):
    outdata[wrspec] = { 
        'wr_spec': wrspec,
        'Tasks': '',
        'Abilities': '',
        'Skills': '',
        'Knowledge': ''
    }

    for ksat in reqs.aggregate([
        {'$lookup': {
            'from': 'work_roles',
            'localField': '_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles.work-role': wrspec}}
    ]):
        ksat_id = ksat['_id']
        ksat_key = key_picker(ksat_id)
        if len(outdata[wrspec][ksat_key]) == 0:
            outdata[wrspec][ksat_key] = f'{ksat_id}'
        else:
            outdata[wrspec][ksat_key] += f', {ksat_id}'

def create_wrspec_dataset(work_roles:list):
    global output_root
    wrspec_data = {}
    wrspec_output_path = os.path.join(output_root, 'wr_spec.min.json')

    # itterate through all work-role names
    for work_role in work_roles:
        query_and_itterate_wrspec(work_role, wrspec_data)

    os.makedirs(output_root, exist_ok=True)
    with open(wrspec_output_path, 'w+') as fd:
        json.dump(list(wrspec_data.values()), fd, sort_keys=False, separators=(',', ':'))

def create_nowrspec_dataset():
    global output_root
    nowrspec_output_path = os.path.join(output_root, 'nowr_spec.min.json')

    wrdata = {
        'Tasks': [],
        'Abilities': [],
        'Skills': [],
        'Knowledge': []
    }

    for ksat in reqs.aggregate([
        {'$lookup': {
            'from': 'work_roles',
            'localField': '_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles': {'$size': 0} }}        
    ]):
        ksat_id = ksat['_id']
        wrdata[key_picker(ksat_id)].append(ksat_id)

    for key in wrdata.keys():
        wrdata[key] = ', '.join(wrdata[key])

    os.makedirs(output_root, exist_ok=True)
    with open(nowrspec_output_path, 'w+') as fd:
        json.dump([wrdata], fd, sort_keys=False, separators=(',', ':'))

def main():
    global error, output_root, log_output_path
    work_roles = list(wrs.distinct('work-role'))

    create_wrspec_dataset(work_roles)
    create_nowrspec_dataset()

if __name__ == "__main__":
    main()