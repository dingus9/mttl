#!/usr/bin/python3

import ast
import os
import re
import sys
import json
import argparse
import pdfrw
import pdfkit
from shutil import copyfile
from pdfrw import PdfReader, PdfWriter, PdfName

log = []

description = "This will create a JQS checklist based on KSATs required by the work role (only that have training)."

work_role_default = None
work_role_file = "WORK-ROLES.json"
spec_file = "SPECIALIZATIONS.json"

HTML_OPTIONS = {
    'dpi': 365,
    'page-size': 'Letter',
    'margin-top': '0.25in',
    'margin-right': '0.25in',
    'margin-bottom': '0.25in',
    'margin-left': '0.25in',
    'minimum-font-size': '10',
    'encoding': 'UTF-8',
    'custom-header': [
        ('Accept-Encoding', 'gzip')
    ],
    'no-outline': None,
}

HTML_TABLE_STYLE = '<style>' \
                'table, th { ' \
                'border: 1px solid black;' \
                'padding: 5px;' \
                'max-width: 2480px;' \
                'width: 100%;' \
                'border-collapse: collapse;' \
                '}' \
                'td {' \
                'border: 1px solid black;' \
                'padding: 5px;' \
                'max-width: 100px;' \
                'overflow: hidden;' \
                'text-overflow: ellipsis;' \
                'white-space: nowrap;' \
                '}' \
                '</style>\n'

initials_header = '<th>Trainee Initials</th>\n    <th>Trainer Initials</th>\n'
empty_cell = '<td></td>\n'
row_tag_open = '<tr>\n'
row_tag_close = '</tr>\n'
table_tag_open = '<table>\n'
table_tag_close = '</table>'
# TODO: Make a command line arg for number of rows for a new template
# TODO: Make a command line arg for number of cols for a new template

ttl_metrics_file = "frontend/src/data/TTLs.min.json"
jqs_directory = "frontend/src/data/JQS"

header_template = "scripts/build/header_template.pdf"
trainer_table_template = "scripts/build/trainer_table_template.pdf"
table_template = "scripts/build/table_template.pdf"

url_regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'


def delete_temp_files(files: list) -> int:
    '''
    Purpose: This will delete the temporary files used to create OJT documents.
    :return: 1 if there is an exception.
    '''
    for file in files:
        try:
            # If the file is in mttl directory, delete it
            if file in os.listdir(os.getcwd()):
                os.remove(file)
        except (OSError, FileNotFoundError, SystemError):
            return 1


def get_workroles() -> list:
    '''
    Purpose: This is responsible for parsing out the current workroles.
    :return: List (file name)
    '''
    f = open(work_role_file, "r")
    contents = f.read()
    dictionary = ast.literal_eval(contents)

    f.close()

    return list(dictionary.keys())


def add_ksat_to_table(jqs_data: list, index: int) -> str:
    '''
    Purpose: This is responsible for modifying the table template for the choosen workrole.
    :return: String (file name)
    '''

    # Declare the writer
    writer = PdfWriter()

    # Create a temp file
    new_table = f"{args.work_role}-table-{index}.pdf"
    copyfile(table_template, new_table)
    print("Created {}".format(new_table))

    # Declare a reader
    reader = PdfReader(new_table)
    reader.Root.AcroForm.update(pdfrw.PdfDict(NeedAppearances=pdfrw.PdfObject('true')))
    writer.addpages(reader.pages)

    # Iterate through the jqs_data
    for i in range(len(jqs_data)):
        page_iterator(jqs_data, writer, 7)

    # Update the writers root to display filable fields in Adobe
    writer.trailer.Root.update(pdfrw.PdfDict(AcroForm=reader.Root.AcroForm))

    writer.write(new_table, trailer=writer.trailer)
    return new_table


def page_iterator(jqs_data: list, writer: object, row_length: int):
    '''
    Purpose: Iterates through each page in the writers page array and calls assign_annot_row
    :return: None
    '''
    for page_index, page in enumerate(writer.pagearray):
        assign_annot_row(jqs_data, page_rows(page.Annots, row_length), page_index)


def assign_annot_row(jqs_data: list, rows: list, page_index: int):
    '''
    Purpose: Loops through each annot row and calls assign_annot_col
    :return: None
    '''
    for ksa_index, row in enumerate(rows):
        assign_annot_col(jqs_data, row, page_index, ksa_index)


def assign_annot_col(jqs_data: list, row: list, page_index: int, ksa_index: int):
    '''
    Purpose: Loops through annot cell of a row(column)
    :return: None
    '''
    row_index = ksa_index * (page_index + 1)
    for j, col in enumerate(row):
        if row_index < len(jqs_data):
            create_value_and_appearance(col, jqs_data[ksa_index * (page_index + 1)][j])


def page_rows(l: list, n: int):
    '''
    Purpose: Breaks up a list based on the n length
    :return: List of lists. Inner list is n long.
    '''
    return [l[i:i+n] for i in range(0, len(l), n)]


def create_value_and_appearance(annot, value):
    '''
    Purpose: This is responsible for assigning the initial Apperance(AP) and default value(V) of a adobe annot node
    :return: None
    '''
    annot.update(pdfrw.PdfDict(NeedAppearances=pdfrw.PdfObject('true')))
    annot.update(pdfrw.PdfDict(AP=value))
    annot.update(pdfrw.PdfDict(V=value))


def add_fields_to_header(checklist_type) -> str:
    '''
    Purpose: This is responsible for modifying the header template for the choosen workrole
    :return: String (file name)
    '''
    outfn = "{}-{}-Header.pdf".format(args.work_role, checklist_type)

    JobCode = "xxxxxx"
    Certifier1 = "Example Johnson"
    TotalPageNum = "6"
    QualStandards = ["EQ", "Q1", "Q2", "Q3"]
    POCEmailAddress = "90IOS.DOT.INBOX@us.af.mil "

    reader = PdfReader(header_template)
    writer = PdfWriter()

    writer.addpages(reader.pages)

    for page in writer.pagearray:
        for annot in page.Annots:
            # Check if the fillable field matches the field name.
            if annot.T == pdfrw.PdfString("(WorkRole1)") or annot.T == pdfrw.PdfString("(WorkRole2)") or annot.T == pdfrw.PdfString("(WorkRole3)"):
                # Function out the apperance and value
                create_value_and_appearance(annot, args.work_role)

            if annot.T == pdfrw.PdfString("(JobCode)"):
                create_value_and_appearance(annot, JobCode)

            if annot.T == pdfrw.PdfString("(Certifier1)") or annot.T == pdfrw.PdfString("(Certifier2)"):
                create_value_and_appearance(annot, Certifier1)

            if annot.T == pdfrw.PdfString("(TotalPageNum)"):
                create_value_and_appearance(annot, TotalPageNum)

            if annot.T == pdfrw.PdfString("(QualStandards)"):
                create_value_and_appearance(annot, ', '.join(QualStandards))

            if annot.T == pdfrw.PdfString("(QualStandards[0])"):
                create_value_and_appearance(annot, QualStandards[0])

            if annot.T == pdfrw.PdfString("(POCEmailAddress)"):
                create_value_and_appearance(annot, POCEmailAddress)

    writer.write(outfn, trailer=writer.trailer)

    return outfn


def field_fixxer(pdf_files, checklist_type):
    '''
    Purpose: This is responsible for adding the Acroform/fields that get lost when using the writer from the library.
    THIS FIXES THE DISPLAY ISSUE WHEN VIEWING IN ADOBE.
    :return: None
    '''
    acroform_dict_field = PdfName('AcroForm')
    fields_array_field = PdfName('Fields')
    root_dict_field = PdfName('Root')
    t_field = PdfName('T')
    dr_field = PdfName('DR')
    font_field = PdfName('Font')

    output_filename = f"{args.work_role}-{checklist_type}.pdf"
    output = PdfWriter()
    page_num = 0
    output_acroform = None
    for pdf in pdf_files:
        input = PdfReader(pdf, verbose=False)
        output.addpages(input.pages)
        if acroform_dict_field in input[root_dict_field].keys():  # Not all PDFs have an AcroForm node
            source_acroform = input[root_dict_field][acroform_dict_field]
            if fields_array_field in source_acroform:
                output_formfields = source_acroform[fields_array_field]
            else:
                output_formfields = []
            field_num = 0
            for form_field in output_formfields:
                old_name = form_field[t_field].replace('(', '').replace(')', '')
                form_field[t_field] = f'FILE_{page_num}_FIELD_{field_num}_{old_name}'
                field_num += 1
            if output_acroform is None:
                # copy the first AcroForm node
                output_acroform = source_acroform
            else:
                for key in source_acroform.keys():
                    # Add new AcroForms keys if output_acroform already existing
                    if key not in output_acroform:
                        output_acroform[key] = source_acroform[key]
                # Add missing font entries in /DR field of source file
                if (dr_field in source_acroform.keys()) and (
                        font_field in source_acroform[dr_field].keys()):
                    if font_field not in output_acroform[dr_field].keys():
                        # if output_acroform is missing entirely the /Font node under an existing /DR, simply add it
                        output_acroform[dr_field][font_field] = source_acroform[dr_field][
                            font_field]
                    else:
                        # else add new fonts only
                        for font_key in source_acroform[dr_field][font_field].keys():
                            if font_key not in output_acroform[dr_field][font_field]:
                                output_acroform[dr_field][font_field][font_key] = \
                                source_acroform[dr_field][font_field][font_key]
            if fields_array_field not in output_acroform:
                output_acroform[fields_array_field] = output_formfields
            else:
                # Add new fields
                output_acroform[fields_array_field] += output_formfields
        page_num += 1
    output.trailer[root_dict_field][acroform_dict_field] = output_acroform
    output.write(os.path.join(jqs_directory, output_filename))
    delete_temp_files(pdf_files)


def parse_work_role_jqs(ttl_data) -> dict:
    '''
    Purpose: This is responsible for creating a list of lists of KSATs that are covered under training
    :return: List
    '''
    jqs_data = []
    try:
        for ksa in ttl_data[args.work_role]:
            if ksa['training']:
                jqs_data.append(
                    (ksa['_id'],
                     ksa['topic'],
                     ksa['description'],
                     re.findall(url_regex, ksa['training'])[0],
                     ' ', ' ', ' ')
                )

        jqs_data.sort(key=lambda x: x[1])
    except Exception as err:
        print("Error: ".format(err))

    return jqs_data


def parse_work_role_ojt(ttl_data) -> dict:
    '''
    Purpose: This is responsible for parsing KSATS based on the following logic:
    1)Training but no Eval
    2)Eval but no Training
    3)No Eval and no Training
    :return: Dictionary
    '''
    ojt_data = {}
    no_training_yes_eval = []
    yes_training_no_eval = []
    no_training_no_eval = []
    try:
        for ksa in ttl_data[args.work_role]:
            # no training but has Eval
            if ksa['training'] == '' and ksa['eval'] != '':
                no_training_yes_eval.append(
                    (f"{ksa['_id']} {ksa['description']}",
                     ksa['eval'],
                     ' ',
                     ' ',
                     ' ')
                )
            # With training but no Eval
            elif ksa['training'] != '' and ksa['eval'] == '':
                yes_training_no_eval.append(
                    (f"{ksa['_id']} {ksa['description']}",
                     ' ',
                     ksa['training'],
                     ' ',
                     ' ')
                )
            # No training and no Eval
            elif ksa['training'] == '' and ksa['eval'] == '':
                no_training_no_eval.append(
                    (f"{ksa['_id']} {ksa['description']}",
                     ' ',
                     ' ',
                     ' ',
                     ' ')
                )

        no_training_yes_eval.sort(key=lambda x: x[1])
        yes_training_no_eval.sort(key=lambda x: x[1])
        no_training_no_eval.sort(key=lambda x: x[1])

        ojt_data = {"KSAT that require OJT because of no Training": no_training_yes_eval,
                    "KSAT that require OJT because of no Evaluations": yes_training_no_eval,
                    "KSAT that require OJT because of no Training and no Evaluations": no_training_no_eval
                    }

    except Exception as err:
        print("Error: ".format(err))

    return ojt_data


def html_cell_make(style, content) -> str:
    '''
    Purpose: Creates a html table cell.
    :return: A string;
    '''
    return f'<td {style}>{content}</td>\n'


def html_header_maker(collection_direction, span, percentage, content) -> str:
    '''
    Purpose: Creates a html table header.
    :return: A string;
    '''
    return f'<th {collection_direction}span="{span}" style="white-space:nowrap width:{percentage}%">{content}</th>\n'


def make_course_table(data) -> str:
    table = HTML_TABLE_STYLE
    # create the header of the table
    table += '<h1 style="text-align:center">Courses</h1>\n'
    table += "".join([
        table_tag_open,
        row_tag_open,
        html_header_maker(collection_direction="row", span=0, percentage=50, content="Course"),
        html_header_maker(collection_direction="row", span=0, percentage=40, content="Date Completed"),
        html_header_maker(collection_direction="row", span=0, percentage=10, content="Instructor initials")
    ])
    # create the content of the table
    for line in data:
        row = "".join([
            row_tag_open,
            html_cell_make(style='style="white-space:normal"', content=line["name"]),
            empty_cell, # cell for date completed
            empty_cell, # cell for instructor initials
            row_tag_close
        ])
        table += row
    table += table_tag_close

    pdf_file = "courses-list.pdf"
    try:
        # Commented out to write html for style trouble shooting
        # with open("courses-list.html", "w") as f:
        #     f.write(table)
        pdfkit.from_string(table, pdf_file, options=HTML_OPTIONS)
    except Exception as err:
        print(f"Error: {err}")

    return pdf_file

def make_html_table(title, data) -> str:
    '''
    Purpose: This is responsible for creating a html table and converts it into a pdf and writes to a pdf.
    :return: A file name;
    '''

    table = HTML_TABLE_STYLE

    table += f'<h1 style="text-align:center">{title}</h1>\n'

    table += table_tag_open
    # Create the table's column headers
    table += row_tag_open
    table += html_header_maker(collection_direction="row", span=2, percentage=40, content="KSAT: Description")
    table += html_header_maker(collection_direction="row", span=2, percentage=10, content="Date Started")
    table += html_header_maker(collection_direction="row", span=2, percentage=10, content="Date Completed")
    # If training is empty, double initial
    if len(data) == 0 or data[0][1] == ' ' and not data[0][2] == ' ':
        table += html_header_maker(collection_direction="row", span=2, percentage=20, content="Training Sign-off")
        table += html_header_maker(collection_direction="col", span=2, percentage=20, content="Eval Sign-off")
        table += row_tag_open
        table += initials_header
        table += row_tag_close
    # If eval is empty, double initial
    elif data[0][2] == ' ' and not data[0][1] == ' ':
        table += html_header_maker(collection_direction="col", span=2, percentage=20, content="Training Sign-off")
        table += html_header_maker(collection_direction="row", span=2, percentage=20, content="Eval Sign-off")
        table += row_tag_open
        table += initials_header
        table += row_tag_close
    # If eval and training is empty, double initial
    elif data[0][1] == ' ' and data[0][2] == ' ':
        table += html_header_maker(collection_direction="col", span=2, percentage=20, content="Training Sign-off")
        table += html_header_maker(collection_direction="col", span=2, percentage=20, content="Eval Sign-off")
        table += row_tag_open
        table += initials_header
        table += initials_header
        table += row_tag_close
    table += row_tag_close

    # Create the table's row data
    for line in data:
        table += row_tag_open
        table += html_cell_make(style='style="white-space:normal"', content=line[0])
        table += empty_cell
        table += empty_cell
        # If eval is empty, initial
        if line[2] == ' ' and not line[1] == ' ':
            table += empty_cell
            table += empty_cell
        else:
            table += html_cell_make(style='', content=line[2])
        # If training is empty, initial
        if line[1] == ' ' and not line[2] == ' ':
            table += empty_cell
            table += empty_cell
        else:
            table += html_cell_make(style='', content=line[1])
        # If eval and training is empty, double initial
        if line[1] == ' ' and line[2] == ' ':
            table += empty_cell
            table += empty_cell
        table += row_tag_close

    table += table_tag_close

    pdf_file = f"ojt-table-{title.replace(' ', '-')}.pdf"
    try:
        # Commented out to write html for style trouble shooting
        # f = open(f'{title.replace(' ', '-')}.html", "w")
        # f.write(table)
        pdfkit.from_string(table, pdf_file, options=HTML_OPTIONS)
    except Exception as err:
        print(f"Error: {err}")

    return pdf_file


def main() -> int:
    '''
    Purpose: This is responsible for collecting all requested input as well as valid data for comparison in order to
    validate each requirement file within the MTTL. Error statements are kept in a list called log.
    :return: Success = 0; errors = 1
    '''
    global args, log

    try:
        if not os.path.isdir(jqs_directory):
            os.makedirs(jqs_directory)
    except OSError:
        return 1

    with open(ttl_metrics_file, "r") as ttl_fp:
        ttl_data = json.load(ttl_fp)
    jqs_data = parse_work_role_jqs(ttl_data)
    if len(jqs_data) <= 0:
        print("No training for {} Work Role.".format(args.work_role))
        # return

    with open(work_role_file) as wr_fp:
        wr_courses = json.load(wr_fp)
    try:
        # check if that role exists in workrole. if not attempt to find it in specializations
        if not wr_courses[args.work_role]:
            with open(spec_file) as sp_fp:
                wr_courses = json.load(sp_fp)
    # if there was a key error attempting to access a workrole and it was not found it may be a specialization
    except KeyError:
        with open(spec_file) as sp_fp:
            wr_courses = json.load(sp_fp)

    # Make JQS checklist
    file_names = []
    checklist_type = "JQS"
    file_names.append(add_fields_to_header(checklist_type))
    for index, page_data in enumerate(page_rows(jqs_data, 30)):
        file_names.append(add_ksat_to_table(page_data, index))
    field_fixxer(file_names, checklist_type)

    # Make OJT checklist
    file_names = []
    checklist_type = "OJT"
    ojt_data = parse_work_role_ojt(ttl_data)
    if len(ojt_data) <= 0:
        print("No OJT training for {} Work Role.".format(args.work_role))
        return
    file_names.append(add_fields_to_header(checklist_type))

    # Make Course list
    try:
        # only add courses if they exist
        if len(wr_courses[args.work_role]["courses"]) > 0:
            file_names.append(make_course_table(wr_courses[args.work_role]["courses"]))
    except KeyError:
        pass

    file_names.append(trainer_table_template)
    for key in ojt_data.keys():
        file_names.append(make_html_table(key, ojt_data.get(key)))
    field_fixxer(file_names, checklist_type)

    return 0


if __name__ == "__main__":
    # Get workroles
    wr_list = []
    wr_list = get_workroles()
    # Now that we have work roles, create arg parse
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-wr", "--work_role", type=str, default=work_role_default, help=wr_list)
    args = parser.parse_args()
    status = main()
    if status == 0:
        msg = "JQS created for {}.".format(args.work_role)
        print(msg)
        log.append(msg)
    else:
        print("Errors creating {} JQS.".format(args.work_role))

    sys.exit(status)
