#!/usr/bin/python3

import os
import sys
import json
import pymongo
from bson.son import SON
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json, convert_lists
import mongo_helpers

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    output_root = 'frontend/src/data'
    output_path = os.path.join(output_root, 'MTTL.min.json')

    # find all ksats and itterate
    mttl = list(reqs.aggregate([
        {'$lookup': {
            'from': 'work_roles',
            'localField': '_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$unset': 'work-roles._id'},
        {'$sort': SON([('_id', 1)])}
    ]))
    for ksat in mttl:
        convert_lists(ksat)
        ksat['work-roles/specializations'] = ksat['work-roles']
        ksat['children'] = '' if 'children' not in ksat else ksat['children']

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as mttfile:
        json.dump(mttl, mttfile, sort_keys=False, separators=(',', ':'))

if __name__ == "__main__":    
    main()