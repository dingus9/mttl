#!/bin/bash

curl -L https://gitlab.com/90cos/training/baselines/baseline-mdbook/-/jobs/artifacts/master/download?job=package_template --output public.zip
unzip public.zip
mv public mdbook_baseline
curl -L https://gitlab.com/90cos/training/baselines/baseline-slides/-/jobs/artifacts/master/download?job=package_template --output public.zip
unzip public.zip
mv public slides_baseline
rm public.zip

cp -r docs/mdbook/* mdbook_baseline/

# install mdbook-mermaid on mdbook_baseline book
mdbook-mermaid install mdbook_baseline/

mdbook build -d ../documentation ./mdbook_baseline
mv slides_baseline documentation/

for n in `find documentation/* -type d -path "*slides"` 
do 
    echo "Copying 'documenation/slides_baseline/*' into '$n'"
    cp -r documentation/slides_baseline/* $n/
done
rm -r documentation/slides_baseline
rm -r mdbook_baseline