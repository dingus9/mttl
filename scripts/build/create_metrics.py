#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
import mongo_helpers

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
wrs = db.work_roles

output_root = 'frontend/src/data'

def update_metrics(metrics:dict, key:str, with_both:int, with_training:int, with_eval:int, total:int, without_training_set:list, without_eval_set:list):
    metrics.update({ key: { 
        'percent-total': (with_both / total) * 100 if total > 0 else 0,
        'percent-training': (with_training / total) * 100 if total > 0 else 0,
        'percent-eval': (with_eval / total) * 100 if total > 0 else 0,
        'ksas-without-training': without_training_set,
        'ksas-without-eval': without_eval_set,
        'count-total': total,
        'count-training-eval': with_both,
        'count-training': with_training,
        'count-eval': with_eval
    }})

def create_mttl_metrics(metrics:dict):
    '''
    create mttl metrics in the 'metrics' dict
    '''
    # query mongodb for various information that we convert into a list of strings
    without_training_set = [ksat['_id'] for ksat in reqs.find({'training': {'$size': 0}}, sort=[('_id', 1)])]
    without_eval_set = [ksat['_id'] for ksat in reqs.find({'eval': {'$size': 0}}, sort=[('_id', 1)])]

    # count all ksats with training and eval
    with_training = reqs.count_documents({'training': {'$not': {'$size': 0}}})
    with_eval = reqs.count_documents({'eval': {'$not': {'$size': 0}}})
    with_both = reqs.count_documents({
            'training': {'$not': {'$size': 0}},
            'eval': {'$not': {'$size': 0}}
        })
    # count all ksats for total
    total = reqs.count_documents({})

    update_metrics(metrics, 'MTTL', with_both, with_training, with_eval, total, without_training_set, without_eval_set)

def append_ttl_metrics(metrics:dict, wrspec:str):
    '''
    create ttl metrics in the 'metrics' dict
    '''
    find_aggregate = [
        {'$lookup': {
            'from': 'work_roles',
            'localField': '_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles.work-role': wrspec}}
    ]
    if 'work-roles' not in metrics:
        metrics['work-roles'] = {}

    # query mongodb for various information that we convert into a list of strings
    without_training_set = [ksat['_id'] for ksat in reqs.aggregate(find_aggregate + [{
        '$match' : {
            'training': {'$size': 0}
        }
    }])]
    without_eval_set = [ksat['_id'] for ksat in reqs.aggregate(find_aggregate + [{
        '$match' : {
            'eval': {'$size': 0}
        }
    }])]

    # count all ksats with training
    with_training = len(list(reqs.aggregate(find_aggregate + [{
            '$match': {
                'training': {'$not': {'$size': 0}}
            }
        }])))
    # count all ksats with eval
    with_eval = len(list(reqs.aggregate(find_aggregate + [{
            '$match': {
                'eval': {'$not': {'$size': 0}}
            }
        }])))
    # count all ksats with eval and training
    with_both = len(list(reqs.aggregate(find_aggregate + [{
            '$match': {
                'training': {'$not': {'$size': 0}},
                'eval': {'$not': {'$size': 0}}
            }
        }])))
    # count all ksats for total
    total = len(list(reqs.aggregate(find_aggregate)))

    update_metrics(metrics['work-roles'], wrspec, with_both, with_training, with_eval, total, without_training_set, without_eval_set)

def create_ttl_metrics(metrics:dict, work_roles:list):
    '''
    Starter function for ttl metrics to pass individual work-roles/specializations into append_ttl_metrics
    '''
    for work_role in work_roles:
        append_ttl_metrics(metrics, work_role)
    

def create_nowrspec_metrics(metrics:dict):
    without_wrspec = len(list(reqs.aggregate([\
        {'$lookup': {
            'from': 'work_roles',
            'localField': '_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles': {'$size': 0} }}
    ])))
    total = reqs.count_documents({})

    metrics.update({ 'nowrspec': {
        'percent-total': (without_wrspec / total) * 100 if total > 0 else 0
    }})

def main():
    global output_root
    
    metrics = {}
    output_path = os.path.join(output_root, 'metrics.min.json')
  
    # # for work_role_path in work_role_paths:
    # with open('WORK-ROLES.json', 'r') as wr_spec_file:
    #     work_roles = list(json.load(wr_spec_file).keys())
    # with open('SPECIALIZATIONS.json', 'r') as wr_spec_file:
    #     specializations = list(json.load(wr_spec_file).keys())

    work_roles = list(wrs.distinct('work-role'))

    create_mttl_metrics(metrics)
    create_ttl_metrics(metrics, work_roles)
    create_nowrspec_metrics(metrics)

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as metrics_file:
        json.dump(metrics, metrics_file, sort_keys=False, separators=(',', ':'))

if __name__ == "__main__":
    main()