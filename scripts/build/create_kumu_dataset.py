#!/usr/bin/python3

import os
import sys
import json
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json

req_path = 'requirements/'
req_ext = '.json'

def key_picker(key: str):
    """
    Helper function that will pick the correct string from the key identifier
    """
    return {
        'K': 'Knowledge',
        'S': 'Skills',
        'A': 'Abilities',
        'T': 'Tasks'
    }[key[0]]

def main():
    elements = []
    connections = []
    mttldata = []
    ksat_paths = get_all_json(req_path, req_ext , [])

    for ksat_path in ksat_paths:
        with open(ksat_path, 'r') as ksa_file:
            mttldata += json.load(ksa_file)

    # add label and type fields and from/to
    for ksat in mttldata:
        key = ksat['_id']
        elements.append({
            'Label': key,
            'Type': key_picker(key),
            'Description': ksat['description'],
            'Topic':ksat['topic'],
            'OPR':ksat['requirement_owner'],
            'Source':ksat['requirement_src']

        })
        if len(ksat['parent']) > 0:
            for parent in ksat['parent']:
                connections.append({
                    'From': key,
                    'To': parent
                })
    
    kumudata = {
        'elements': elements,
        'connections': connections
    }
    
    with open('kumu-data.json', 'w') as kumufile:
        json.dump(kumudata, kumufile, sort_keys=False, indent=4)


if __name__ == "__main__":
    main()