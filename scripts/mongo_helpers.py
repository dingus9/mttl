def find_array_not_empty(collection, field: str):
    '''
    MongoDB Query function to find documents with field (array type) that is not empty
    '''
    return collection.find({
        f'{field}' : {'$not':{'$size':0}}
    }, sort=[('_id', 1)])

def find_id_and_append_array(collection, find_id: str, update_field: str, value: [str, dict], upsert:bool=False):
    '''
    MongoDB Query function to find one document with id
    '''
    return collection.update_one(
        { '_id': find_id },
        { '$push': { update_field: value }},
        upsert=upsert
    )

def find_id_and_modify(collection, find_id: str, update_field: str, value: [str, list], upsert: bool=False):
    '''
    MongoDB Query function to find one document with id
    '''
    return collection.update_one(
        { '_id': find_id },
        { '$set': { update_field: value }},
        upsert=upsert
    ) 