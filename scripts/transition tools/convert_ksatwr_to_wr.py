#!/usr/bin/python3

import os
import re
import sys
import pymongo
import argparse
import json
from pprint import pprint
from datetime import date
from bson.objectid import ObjectId

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links

def get_proficiency(wr_name: str, wr: list):
    for item in wr:
        if item['_id'] == wr_name and 'proficiency' in item:
            return item['proficiency']
        else:
            return ''

def main():
    for wr in list(reqs.distinct('work-roles._id')) + list(reqs.distinct('specializations._id')):
        wr_list = []
        for ksat in list(reqs.find({'$or': [{'work-roles._id': wr}, {'specializations._id': wr}]})):
            wr_list.append({
                '_id': ksat['_id'],
                'work-role': wr,
                'proficiency': get_proficiency(wr, list(ksat['work-roles']) + list(ksat['specializations']))
            })

        with open(f'work-roles/{wr}.json', 'w') as fd:
            json.dump(wr_list, fd, sort_keys=False, indent=4)

if __name__ == "__main__":
    main()