#!/bin/bash
whoami
declare -a evalrepos=(
    "https://gitlab.com/90cos/public/evaluations/preparatory/basic-dev.git"
    "https://gitlab.com/90cos/public/evaluations/preparatory/basic-po.git"
    "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-linux.git"
    "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-windows.git"
    )
declare -a trainrepos=(
    "https://gitlab.com/90cos/training/modules/python.git"
    "https://gitlab.com/90cos/training/modules/c-programming.git"
    "https://gitlab.com/90cos/training/modules/reverse-engineering.git"
    "https://gitlab.com/90cos/training/modules/algorithms.git"
    "https://gitlab.com/90cos/training/modules/powershell.git"
    "https://gitlab.com/90cos/training/modules/debugging.git"
    "https://gitlab.com/90cos/training/modules/pseudocode.git"
    "https://gitlab.com/90cos/training/modules/cpp-programming.git"
    "https://gitlab.com/90cos/training/modules/introduction-to-git.git"
    "https://gitlab.com/90cos/training/modules/assembly.git"
    "https://gitlab.com/90cos/training/modules/network-programming.git"
    "https://gitlab.com/90cos/training/courses/external/scrum.org-professional-scrum-product-owner-i.git"
    "https://gitlab.com/90cos/training/courses/external/actp-linux.git"
    "https://gitlab.com/90cos/training/material/see-basics.git"
)
mkdir eval-repos
cd eval-repos
for i in "${evalrepos[@]}"
do
    git clone "$i"
done
cd ..
mkdir training-repos
cd training-repos
for i in "${trainrepos[@]}"
do
    git clone "$i"
done
