#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from mongo_helpers import find_array_not_empty, find_id_and_modify

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def update_trn_eval(reqs:object, rls:object, ksat:dict, key:str, link_name_key:str):
    new_list = []

    # find all rel_link mapping to specific ksat
    rel_link_mapping_ids = rls.aggregate([
        { #this will find all rel_link documents of _ids in training or eval ksat fields
            '$match': {
                '_id': {
                    '$in':ksat[key]
                }
            }
        },
        { # we only want the KSATs mapping for this specific ksat (remember the rel_link document has all mapping for that rel_link)
            '$project': {
                'name': f'${link_name_key}',
                'KSATs':{
                    '$filter': {
                        'input': '$KSATs',
                        'as': 'this_ksat',
                        'cond': {
                            '$eq': ['$$this_ksat.ksat_id', ksat['_id']]
                        }
                    }
                }
            }
        }
    ])

    for item in rel_link_mapping_ids:
        if len(item['KSATs']) == 0:
            continue
        mapping = item['KSATs'][0]
        url = mapping['url']
        prof = mapping['item_proficiency'] if len(mapping['item_proficiency']) > 0 else 'N/A'
        new_list.append(f'<a target="_blank" rel="noopener noreferrer" href="{url}" title="proficiency {prof}">{item["name"]}</a>')
    find_id_and_modify(reqs, ksat['_id'], key, new_list, upsert=False) 

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    for ksat in find_array_not_empty(reqs, 'training'):
        update_trn_eval(reqs, rls, ksat, 'training', 'subject')
    
    for ksat in find_array_not_empty(reqs, 'eval'):
        update_trn_eval(reqs, rls, ksat, 'eval', 'question_name')

if __name__ == "__main__":
    main()