#!/usr/bin/python3

import os
import sys
import json
import jsonschema
import argparse
from bson.objectid import ObjectId
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from json_templates import ksat_wr_spec_template

def get_work_roles(paths: list) -> list:
    '''
    return a list of all the work-roles/specializations
    '''
    wr_list = []
    for path in paths:
        with open(path, 'r') as path_file:
            wr_list += list(json.load(path_file).keys())
    return wr_list

def validate_insert_data(data: dict, schema_func):
    '''
    validate all insertion data
    '''
    wr_list = get_work_roles(['WORK-ROLES.json', 'SPECIALIZATIONS.json'])

    try: 
        jsonschema.validate(instance=data, schema=schema_func(wr_list))
    except jsonschema.exceptions.ValidationError as err:
        print(err)

def main():
    parser = argparse.ArgumentParser(description='Validate rel-link JSON files')
    parser.add_argument('path', metavar='PATH', type=str, help='path to the rel-link directory')
    parsed_args = parser.parse_args()

    for dirName, subdirList, fileList in os.walk(parsed_args.path):
        for fileName in fileList:
            with open(os.path.join(dirName, fileName)) as fd:
                data = json.load(fd)
            validate_insert_data(data, ksat_wr_spec_template)


if __name__ == "__main__":
    main()