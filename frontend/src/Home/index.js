import React, {Component} from 'react'
import {Route, NavLink, BrowserRouter} from "react-router-dom";
import Table from '../Components/Table'
import {YELLOW, BLUE, GREEN} from '../helpers'
import './Home.scss'

const curr_path = window.location.pathname;

let ttl_columns = [
    {field: '_id', title: 'ID', sortable: true},
    {field: 'requirement_src', title: 'Source', sortable: true},
    {field: 'requirement_owner', title: 'Owner', sortable: true},
    {field: 'parent', title: 'Parent(s)', class: "children_description", sortable: true},
    {field: 'description', title: 'Description', class: "header_description", sortable: true},
    {field: 'children', title: 'Children', class: "children_description", sortable: true},
    {field: 'work-roles/specializations', title: 'Work Roles/Specializations', sortable: true},
    {field: 'topic', title: 'Topic', sortable: true},
    {field: 'proficiency', title: 'Proficiency', sortable: true},
    {field: 'training', title: 'Training Covered', class: "extra_header_description", sortable: true},
    {field: 'eval', title: 'Eval Covered', class: "extra_header_description", sortable: true},
    {field: 'comments', title: 'Comments', class: "extra_header_description", sortable: true}
]

let addKSAT = "https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Add+KSAT"
let modKSAT = "https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Modify+KSAT"
let bugReport = "https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Bug"

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { width: 0, height: 0 };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);  
    }
    
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    get_all_table_routes(data) {
        let nav_list = [];
        let route_list = [];

        for(let [key, val] of Object.entries(data)) {
            nav_list.push(<li key={key.toLowerCase()}><NavLink to={`${curr_path + key.toLowerCase()}`} activeClassName='is-active'>{key}</NavLink></li>)
            route_list.push(<Route 
                key={key.toLowerCase()}
                path={`${curr_path + key.toLowerCase()}`} 
                render={props => <Table 
                    {...props}
                    name={key.toLowerCase()} 
                    title={key.replace(/-/g, ' ')}
                    titleStyle={{ marginLeft:"-210px" }}
                    data={val} 
                    height={1}
                    columns={ttl_columns}
                    metrics={this.props.metrics_data['work-roles'][key] || this.props.metrics_data['specializations'][key]}
                    rowStyle={true}
                    hidden_cols={['requirement_src', 'requirement_owner', 'work-roles/specializations', 'eval', 'parent', 'children']}
                />}
            />)
        }

        return [nav_list, route_list];
    }

    render() {
        const links_list = this.get_all_table_routes(this.props.ttl_data)

        return (
            <BrowserRouter>
            <div>
                <div className="feedbackButtonGroup">
                    <a className="feedbackButton" target="_blank" rel="noopener noreferrer" href={addKSAT}>Add KSAT</a>
                    <a className="feedbackButton" target="_blank" rel="noopener noreferrer" href={modKSAT}>Modify KSAT</a>
                    <a className="feedbackButton" target="_blank" rel="noopener noreferrer" href={bugReport}>Report a Bug</a>
                </div>
                <div className="legend_column">
                    <div className="legend_group">
                        <div style={{backgroundColor: BLUE}} className="legend_icon"></div>
                        <p>Training Only</p>
                    </div>
                    <div className="legend_group">
                        <div style={{backgroundColor: YELLOW}} className="legend_icon"></div>
                        <p>Evaluation Only</p>
                    </div>
                    <div className="legend_group">
                        <div  style={{backgroundColor: GREEN}} className="legend_icon"></div>
                        <p>Evaluation and Training</p>
                    </div>
                </div>
                <div className="taskListTables">
                    <div className="tableSelect">
                        <ul>
                            <li><NavLink exact to={`${curr_path}`} activeClassName='is-active'>MTTL</NavLink></li>
                            {links_list[0]}
                        </ul>
                    </div>
                    <div className="tableContent">
                        <Route 
                            exact path={`${curr_path}`}
                            render={props => <Table 
                                {...props}
                                name="mttl" 
                                title="MTTL" 
                                height={1}
                                data={this.props.mttl_data}
                                metrics={this.props.metrics_data['MTTL']}
                                rowStyle={true} 
                            />}
                        />
                        {links_list[1]}                  
                    </div>      
                </div>
            </div>
            </BrowserRouter>
        );
    }
}

export default Home;