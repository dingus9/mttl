import React, {Component} from 'react';
import {Route, NavLink, HashRouter} from "react-router-dom";
import logo from './logo.svg';
import forkMe from './fmog.png'
import './App.scss';

import Home from "./Home";
import Metrics from "./Metrics";
import Roadmap from "./Roadmap";

import * as mttl_data from './data/MTTL.min.json';
import * as ttl_data from './data/TTLs.min.json';
import * as nowr_spec from './data/nowr_spec.min.json';
import * as wr_spec from './data/wr_spec.min.json';
import * as metrics_data from './data/metrics.min.json';

const repo = "https://gitlab.com/90cos/mttl"
const docs = "https://90cos.gitlab.io/mttl/documentation"
const lessons = "https://gitlab.com/90cos/training/modules"
const study = "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study"
const kumu = "https://embed.kumu.io/2e6c3a7ed30c0a4e248b51b41382d736"


class App extends Component {
  state = { isTop: true };

  componentDidMount = () => {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = e => {
    let scrollTop = e.srcElement.all[0].scrollTop;
    if (scrollTop > 0) {
      this.setState({isTop: false});
    }
    else {
      this.setState({isTop: true});
    }
  }

  render() {
    return (
      <HashRouter>
        <div>
          <div className={this.state.isTop ? "headerBar" : "headerBar active"}>
            <a className="forkMe" href={repo}>
                <img src={forkMe} alt={forkMe} />
            </a>
            <img src={logo} className="App-logo" alt="logo" />
            <NavLink className="site-title" to="/">90 COS Master Training Task List</NavLink>
            <div>
              <ul className="header">
                <li><NavLink exact to="/" activeClassName='is-active'>Home</NavLink></li>
                <li><a target="_blank" rel="noopener noreferrer" href={docs}>Docs</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href={kumu}>Visualization</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href={study}>Study</a></li>
                <li><a target="_blank" rel="noopener noreferrer" href={lessons}>Training</a></li>
                <li><NavLink exact to="/roadmap" activeClassName='is-active'>Roadmap</NavLink></li>
                <li><NavLink exact to="/metrics" activeClassName='is-active'>Metrics</NavLink></li>
              </ul>
            </div>
          </div>
          <div className="content">
            <Route 
              exact path="/" 
              render={props => <Home {...props} 
                mttl_data={mttl_data.default}
                ttl_data={ttl_data.default}
                metrics_data={metrics_data.default}
              />}
            />
            <Route 
              path="/metrics" 
              render={props => <Metrics {...props}
                metrics_data={metrics_data.default}
                nowr_spec={nowr_spec.default}
                wr_spec={wr_spec.default}
              />}
            />
            <Route path="/Roadmap" component={Roadmap}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
