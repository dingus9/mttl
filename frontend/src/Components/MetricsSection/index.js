import React, {Component} from 'react'
import './MetricsSection.scss'

import MetricsBar from '../MetricsBar'

class MetricsSection extends Component {
    render() {        
        return (
            <div className="coverage_container">
                <h3>{this.props.title}</h3>
                <div className="item_metrics_container">
                    <div className="labels_container">
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-training-eval"></div>
                                <h6>Training and Eval</h6>
                            </div>
                            <p>{this.props.data['count-training-eval']}/{this.props.data['count-total']}</p>
                        </div>
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-training"></div>
                                <h6>Training</h6>
                            </div>
                            <p>{this.props.data['count-training']}/{this.props.data['count-total']}</p>
                        </div>
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-eval"></div>
                                <h6>Eval</h6>
                            </div>
                            <p>{this.props.data['count-eval']}/{this.props.data['count-total']}</p>
                        </div>
                    </div>
                    <div className="metrics_container">
                        <MetricsBar id={this.props.name + "_wt"} data={this.props.data} />
                    </div>
                </div>
            </div>
        );
    }
}

export default MetricsSection;