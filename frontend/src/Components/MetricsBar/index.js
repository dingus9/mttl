import React, {Component} from 'react'
import './MetricsBar.scss'
import ProgressBar from 'react-bootstrap/ProgressBar'

class MetricsBar extends Component {
    createProgressBar = (data) => {
        let percent_data = [
            {'percent': data['percent-total'], 'type': 'training-eval'}, 
            {'percent': data['percent-training'], 'type': 'training'}, 
            {'percent': data['percent-eval'], 'type': 'eval'}
        ];
        // sort the list <
        percent_data.sort((a, b)=> {return a['percent']-b['percent']});

        let sum = 0;
        let progressBars = []
        percent_data.forEach((val, i, arr) => {
            let percent = Math.round(val['percent']);
            const level = Math.round(percent - sum);
            progressBars.push(<ProgressBar animated striped variant={val['type']} now={level} label={`${level}%`} key={i+1} />);
            if (i === 0) {
                sum = Math.round(val['percent'])
            }
        })

        return (
            <ProgressBar>
                { progressBars }
            </ProgressBar>
        );
    } 

    render() {
        return (
            <div id="metrics_bar">
                {this.createProgressBar(this.props.data)}
            </div>
        );
    }
}

export default MetricsBar;