import React from 'react';
import "./LessonDisplay.css"
import * as arrow from "../../assets/arrow.png"

class CourseDisplay extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            isVisible : false
        };
        this.ToggleDisplay = this.ToggleDisplay.bind(this);
    }
    ToggleDisplay(){
        this.setState({isVisible: !this.state.isVisible})
    }
    render(){
        return(
            <div className="CourseList">
                {"lessons" in this.props.course &&
                <img className="drop_arrow" id={this.state.isVisible ? "rotated" : "unrotated"} src={arrow} alt="dropdownarrow" onClick={this.ToggleDisplay}/>/*inline conditional*/}
                <a href={this.props.course.url}>{this.props.course.name}</a>
                {"lessons" in this.props.course &&/*inline conditional*/
                <div className="ModuleList" style={{display: this.state.isVisible ? "block" : "none"}}>
                    <table><tbody>
                        {this.props.course.lessons.map(module => (
                            <tr>
                                <td><a href={module.url}>{module.name}</a></td>
                            </tr>
                        ))}
                    </tbody></table>
                </div>}
            </div>
        )
    }
}

export default CourseDisplay
