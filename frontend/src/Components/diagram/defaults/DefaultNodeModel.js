import _ from 'lodash';
import { NodeModel } from '../Common';
import { AbstractInstanceFactory } from '../AbstractInstanceFactory';
import 'react-toastify/dist/ReactToastify.css';

export class DefaultNodeInstanceFactory extends AbstractInstanceFactory {
  constructor() {
    super('DefaultNodeModel');
  }

  getInstance() {
    return new DefaultNodeModel();
  }
}

export class DefaultNodeModel extends NodeModel {
  constructor(name = 'Untitled', color = 'rgb(0,192,255)', phase, nodePos, viewer, itemList) {
    super('default');
    this.name = name;
    this.color = color;
    this.phase = phase;
    this.nodePos = nodePos;
    this.viewer = viewer;
    this.itemList = itemList;
  }

  deSerialize(object) {
    super.deSerialize(object);
    this.name = object.name;
    this.color = object.color;
  }

  serialize() {
    return _.merge(super.serialize(), {
      name: this.name,
      color: this.color,
    });
  }

  getInPorts() {
    return _.filter(this.ports,(portModel) => portModel.in);
  }

  getOutPorts() {
    return _.filter(this.ports,(portModel) => !portModel.in);
  }
}
