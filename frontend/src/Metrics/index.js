import React, {Component} from 'react';
// import Table from '../Components/Table'
import './Metrics.scss';

import MetricsSection from '../Components/MetricsSection';

// let wr_spec_columns = [
//     {field: 'wr_spec', title: 'Work Role/Specialization', sortable: true},
//     {field: 'Tasks', title: 'Tasks'},
//     {field: 'Abilities', title: 'Abilities'},
//     {field: 'Skills', title: 'Skills'},
//     {field: 'Knowledge', title: 'Knowledge'},
// ];
// let nowr_spec_columns = [
//     {field: 'Tasks', title: 'Tasks'},
//     {field: 'Abilities', title: 'Abilities'},
//     {field: 'Skills', title: 'Skills'},
//     {field: 'Knowledge', title: 'Knowledge'},
// ];

class Metrics extends Component {
    getMetricsSections = (data) => {
        let section_list = [];
        for(let [key, val] of Object.entries(data)) {
            section_list.push(<MetricsSection name={key} title={key.replace(/-/g, ' ')} data={val} key={key} />)
        }
        return section_list;
    }

    render() {
        const data = this.props.metrics_data;
        let wr_section_list = this.getMetricsSections(data['work-roles']);
        // let spec_section_list = this.getMetricsSections(data['specializations']);

        return (
            <div>
                <div className="metrics">
                    <h1>Metrics</h1>
                    <MetricsSection name="mttl" title="MTTL Coverage" data={data.MTTL} />
                </div>
                <div className="metrics">
                    <h1>Work Roles</h1>
                    {wr_section_list}
                </div>
                {/* <div className="metrics">
                    <h1>Specializations</h1>
                    {spec_section_list}
                </div> */}
                
                {/* <Table name="wrspec" title="Work Roles/Specializations" data={this.props.wr_spec} columns={wr_spec_columns} />
                <Table name="nowrspec" title="KSATs without Work Roles/Specializations" data={this.props.nowr_spec} columns={nowr_spec_columns} metrics={this.props.metrics_data['nowrspec']} /> */}
            </div>
        );
    }
}

export default Metrics;